package dao;
import static Utils.DBUtils.fetchConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.*;

import POJO.users;

public class UserDao {

	
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4;
	
	
	public UserDao() throws ClassNotFoundException, SQLException 
	{
		cn = fetchConnection();
		String sql = "insert into users values(default,?,?,?,?,?,?,?)";
		pst1 = cn.prepareStatement(sql);
		String sql1="select * from users where email=? and password=? ";
		pst2= cn.prepareStatement(sql1);
		String sql2= "select * from polls where created_by=?";
		pst3= cn.prepareStatement(sql2);
	}
	
		
		public String add(users newUser) throws SQLException {
			
			
			pst1.setString(1, newUser.getEmail());
			pst1.setString(2, newUser.getPassword());
			pst1.setString(3, newUser.getMobile());
			pst1.setString(4, newUser.getName());
			pst1.setString(5, newUser.getGender());
			pst1.setString(6, newUser.getAddress());
			pst1.setDate(7,Date.valueOf(newUser.getBirth_date()));
			
			int updateCount = pst1.executeUpdate();
			if (updateCount == 1)
				return "details added....";

			return "Adding  failed...";
		}


		public users authenticateUser(String email, String password) throws SQLException 
		{
			users u =null;
			pst2.setString(1, email );
			pst2.setString(2, password );
			System.out.println(email);
			System.out.println(password);
			ResultSet rst = pst2.executeQuery();
			while(rst.next()) {
				u =	new users(rst.getInt(1) ,rst.getString(2) ,rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6),rst.getString(7),rst.getDate(8).toString());
				
			}
				return u;
									
			}
		


		public List<polls> ListUserPoll(int id) throws SQLException 
		{
			List<polls> p = new ArrayList<polls>();
			
			try(ResultSet rst1 = pst3.executeQuery())
			{	
				while(rst1.next())
				{
					p.add(new polls(rst1.getInt(1), rst1.getString(2),rst1.getDate(3).toString(),rst1.getDate(4).toString(),rst1.getInt(5)));
				}	
			}
			return p;
			
		}


		public void addPoll(String q1, String o1, String o2, String o3, String o4) 
		{
			
			
		}


	
		
		
	

}
